import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  Button,
} from "react-native";
import { useState, useEffect, useRef, useMemo, Component } from "react";
// 5
// View, Text, Image, ScrollView, TextInput, Button

export default function App() {
  const [text, setText] = useState("");
  const [count, setCount] = useState(0);
  return (
    <View style={[styles.container]}>
      {/* <Text>Hello, World</Text> */}
      {/* <Image
        style={{ width: 100, height: 100 }}
        source={require("./assets/favicon.png")}
      /> */}
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#8C9459",
          height: 400,
        }}
      >
        <View style={{ borderWidth: 1, flex: 1, alignItems: "flex-end" }}>
          <Image
            style={{ width: 100, height: 100 }}
            resizeMode="center"
            source={{
              uri: "https://icon-library.com/images/random-icon-generator/random-icon-generator-7.jpg",
            }}
          />
        </View>
        <View style={{ borderWidth: 1, flex: 1, justifyContent: "flex-end" }}>
          <Image
            style={{ width: 100, height: 100 }}
            resizeMode="center"
            source={{
              uri: "https://icon-library.com/images/random-icon-generator/random-icon-generator-7.jpg",
            }}
          />
        </View>
      </View>
      <View style={{ width: 300 }}>
        <TextInput
          style={{ borderWidth: 1, fontSize: 24 }}
          onChangeText={(value) => {
            console.log("Value: ", value);
            setText(value);
          }}
        />
        <Text style={{ color: "red", fontSize: 36 }}>{text}</Text>
        <Text style={{ fontSize: 36 }}>{count}</Text>
      </View>

      {/* <AppClass cobaText={text} /> */}
      <AppFunction cobaText={text} buttonAction={(v) => setCount(count + v)} />
    </View>
  );
}

class AppClass extends Component {
  render() {
    return (
      <View>
        <Text>INI DARI CLASSS</Text>
        <Text>{this.props.cobaText}</Text>
      </View>
    );
  }
}

const AppFunction = ({ cobaText, buttonAction }) => {
  return (
    <View>
      <Text>INI DARI FUNCTION</Text>
      <Text>{cobaText}</Text>
      <Button title="INI BUTTON" onPress={() => buttonAction(5)} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
