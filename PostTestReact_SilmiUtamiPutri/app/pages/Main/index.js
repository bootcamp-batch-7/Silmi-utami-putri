import { View } from "react-native";
import React from "react";
import Text from "../../components/Text";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();

import Home from "../Home";
import Profile from "../Profile";
import { HomeIcon, ProfileIcon, TaskIcon, PerformIcon } from "../../assets/svg";

export default function Main({ navigation, router }) {
  return (
    <View style={{ flex: 1 }}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarActiveTintColor: "#04325F",
          tabBarInactiveTintColor: "#CED1D4",
          tabBarIcon: ({ focused, color, size }) => {
            let tabsIcon = {
              Home: <HomeIcon fill={color} width={size} height={size} />,
              Profile: <ProfileIcon fill={color} width={size} height={size} />,
            };
            return <View>{tabsIcon[route.name]}</View>;
          },
          tabBarLabel: ({ focused, color }) => (
            <Text bold={focused} fontSize={10} color={color}>
              {route.name}
            </Text>
          ),
          tabBarStyle: {
            paddingBottom: 10,
            paddingTop: 10,
            height: 70,
          },
        })}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </View>
  );
}
