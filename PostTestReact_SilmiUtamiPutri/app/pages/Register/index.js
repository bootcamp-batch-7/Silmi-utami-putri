import {
    View,
    ImageBackground,
    ActivityIndicator,
    TouchableOpacity,
    TextInput,
    ScrollView
} from "react-native";
import React, { useState, useEffect } from "react";
import Text from "../../components/Text";
import images from "../../assets/img";
import { EyeIcon, EyeSlashIcon } from "../../assets/svg";
import { Fonts, WIDTH, HEIGHT } from "../../assets/styles";
import Satellite from "../../services/satellite";

export default function Register({ navigation, router }) {
    const [isEnable, setIsEnable] = useState(true);
    const [showPass, setShowPass] = useState(true);


    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [NIK, setNIK] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [errorName, setErrorName] = useState("");
    const [errorEmail, setErrorEmail] = useState("");
    const [errorPhone, setErrorPhone] = useState("");
    const [errorNIK, setErrorNIK] = useState("");
    const [errorPassword, setErrorPassword] = useState("");
    const [errorConfirmPassword, setErrorConfirmPassword] = useState("");

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (
            name !== "" &&
            email !== "" &&
            phone !== "" &&
            NIK !== "" &&
            password !== "" &&
            confirmPassword !== "" &&
            errorName === "" &&
            errorEmail === "" &&
            errorPhone === "" &&
            errorNIK === "" &&
            errorPassword === "" &&
            errorConfirmPassword === ""
        ) {
            setIsEnable(false);
        } else {
            setIsEnable(true);
        }
    }, [
        name,
        email,
        phone,
        NIK,
        password,
        confirmPassword,
        errorName,
        errorEmail,
        errorPhone,
        errorNIK,
        errorPassword,
        errorConfirmPassword,
    ]);

    const onSubmit = async () => {
        setIsLoading(true);
        const body = {
            doSendRegister: {
                name: name,
                email: email,
                phoneNumber: phone,
                password: password,
                nik: NIK
            }
        };

        console.log(JSON.stringify(body, null, 2));
        Satellite.post("auth/register", body)
            .then((response) => {
                if (response.status === 200) {
                    console.log("Response", response)
                    navigation.navigate("Login")
                } else {
                    console.log("Error", response.message)
                    setErrorEmail("Email sudah digunakan")
                }
            })
            .catch((error) => {
                console.log("Error", error.message)
                setErrorEmail("Email sudah digunakan")
            })
            .finally(() => {
                setTimeout(() => {
                    setIsLoading(false);
                }, 1000)

            })


        // You can perform validation checks here, for example:
        if (password !== confirmPassword) {
            setErrorConfirmPassword("Passwords do not match");
            setIsLoading(false);
            return;
        }

        // Your registration API call or backend logic goes here...

        console.log("User Data", userData);

        // Reset loading state when the form is submitted.
        setIsLoading(false);


    };

    return (
        <ImageBackground
            source={images.BG_AUTH}
            resizeMode="cover"
            style={{ width: WIDTH, height: HEIGHT }}
        >
            <ScrollView>
                <View
                    style={{
                        marginTop: 90,
                        marginHorizontal: 16,
                    }}
                >
                    {/* Name Section */}
                    <View>
                        <Text color={"#FFF"} fontSize={16} bold>
                            Name
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorName ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                            }}
                        >
                            <TextInput
                                value={name}
                                placeholder={"Enter Your Name"}
                                placeholderTextColor={"#D3D3D3"}
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                }}
                                onChangeText={(value) => {
                                    setName(value);

                                    if (value === "") {
                                        setErrorName("Name must be filled in");
                                        return;
                                    }

                                    setErrorName("");
                                }}
                            />
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorName}
                        </Text>
                    </View>
                    {/* Email Section */}
                    <View style={{ marginTop: 27 }}>
                        <Text color={"#FFF"} fontSize={16} bold>
                            Email
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorEmail ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                            }}
                        >
                            <TextInput
                                value={email}
                                placeholder={"Enter Your Email"}
                                placeholderTextColor={"#D3D3D3"}
                                keyboardType={"email-address"}
                                autoCapitalize={"none"}
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                }}
                                onChangeText={(value) => {
                                    setEmail(value);
                                    const emailRegex =
                                        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

                                    if (value === "") {
                                        setErrorEmail("Email must be filled in");
                                        return;
                                    }
                                    if (!emailRegex.test(value)) {
                                        setErrorEmail("Invalid email address");
                                        return;
                                    }

                                    setErrorEmail("");
                                }}
                            />
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorEmail}
                        </Text>
                    </View>
                    {/* Phone Section */}
                    <View style={{ marginTop: 27 }}>
                        <Text color={"#FFF"} fontSize={16} bold>
                            Phone
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorPhone ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                            }}
                        >
                            <TextInput
                                value={phone}
                                placeholder={"Enter Your Phone Number"}
                                placeholderTextColor={"#D3D3D3"}
                                keyboardType={"numeric"} // Set keyboardType to 'numeric'
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                }}
                                onChangeText={(value) => {
                                    // Ensure only numeric input is allowed
                                    const numericValue = value.replace(/[^0-9]/g, "");
                                    setPhone(numericValue);

                                    if (numericValue === "") {
                                        setErrorPhone("Phone number must be filled in");
                                        return;
                                    }

                                    setErrorPhone("");
                                }}
                            />
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorPhone}
                        </Text>
                    </View>
                    {/* NIK Section */}
                    <View style={{ marginTop: 27 }}>
                        <Text color={"#FFF"} fontSize={16} bold>
                            NIK
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorNIK ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                            }}
                        >
                            <TextInput
                                value={NIK}
                                placeholder={"Enter Your NIK"}
                                placeholderTextColor={"#D3D3D3"}
                                keyboardType={"numeric"} // Set keyboardType to 'numeric'
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                }}
                                onChangeText={(value) => {
                                    // Ensure the NIK has exactly 16 digits
                                    const numericValue = value.replace(/[^0-9]/g, "");
                                    if (numericValue.length !== 16) {
                                        setErrorNIK("NIK must be exactly 16 digits");
                                    } else {
                                        setErrorNIK("");
                                    }

                                    setNIK(numericValue);
                                }}
                            />
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorNIK}
                        </Text>
                    </View>

                    {/* Password Section */}
                    <View style={{ marginTop: 27 }}>
                        <Text color={"#FFF"} fontSize={16} bold>
                            Password
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorPassword ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                                flexDirection: "row",
                                alignItems: "center",
                            }}
                        >
                            <TextInput
                                value={password}
                                placeholder={"Password"}
                                secureTextEntry={showPass}
                                placeholderTextColor={"#D3D3D3"}
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                    flex: 1,
                                }}
                                onChangeText={(value) => {
                                    setPassword(value);

                                    if (value === "") {
                                        setErrorPassword("Password must be filled in");
                                        return;
                                    }

                                    setErrorPassword("");
                                }}
                            />
                            <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                                {showPass ? (
                                    <EyeIcon width={20} height={20} stroke={"#FFF"} />
                                ) : (
                                    <EyeSlashIcon width={20} height={20} stroke={"#FFF"} />
                                )}
                            </TouchableOpacity>
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorPassword}
                        </Text>
                    </View>
                    {/* Confirm Password Section */}
                    <View style={{ marginTop: 27 }}>
                        <Text color={"#FFF"} fontSize={16} bold>
                            Confirm Password
                        </Text>
                        <View
                            style={{
                                borderWidth: 1,
                                marginTop: 8,
                                borderColor: errorConfirmPassword ? "#EA8685" : "#132040",
                                borderRadius: 8,
                                padding: 12,
                                backgroundColor: "#273C75",
                                flexDirection: "row",
                                alignItems: "center",
                            }}
                        >
                            <TextInput
                                value={confirmPassword}
                                placeholder={"Confirm Password"}
                                secureTextEntry={showPass}
                                placeholderTextColor={"#D3D3D3"}
                                style={{
                                    color: "#FFF",
                                    fontFamily: Fonts.Nunito.Bold,
                                    fontSize: 16,
                                    flex: 1,
                                }}
                                onChangeText={(value) => {
                                    setConfirmPassword(value);

                                    if (value === "") {
                                        setErrorConfirmPassword("Confirm Password must be filled in");
                                        return;
                                    }

                                    setErrorConfirmPassword("");
                                }}
                            />
                            <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                                {showPass ? (
                                    <EyeIcon width={20} height={20} stroke={"#FFF"} />
                                ) : (
                                    <EyeSlashIcon width={20} height={20} stroke={"#FFF"} />
                                )}
                            </TouchableOpacity>
                        </View>
                        <Text
                            color="#EA8685"
                            fontSize={10}
                            style={{ textAlign: "right", marginRight: 8, marginTop: 5 }}
                        >
                            {errorConfirmPassword}
                        </Text>
                    </View>
                    {/* Button Submit */}
                    <TouchableOpacity
                        disabled={isEnable || isLoading}
                        onPress={onSubmit}
                        style={{
                            backgroundColor: "#18DCFF",
                            borderRadius: 8,
                            paddingVertical: 12,
                            alignItems: "center",
                            marginTop: 20,
                            opacity: isEnable || isLoading ? 0.5 : 1,
                        }}
                    >
                        {isLoading ? (
                            <ActivityIndicator size="small" color="#261A31" />
                        ) : (
                            <Text color={"#261A31"} fontSize={16} bold>
                                Register
                            </Text>
                        )}
                    </TouchableOpacity>

                    {/* <View style={{ alignItems: "center", marginTop: 20 }}>
            <Text color={"#FFF"} bold>
              Already have an account?{" "}
              <Text bold color={"#F6E58D"} onPress={() => {}}>
                Sign In
              </Text>
            </Text>
          </View> */}
                </View>
            </ScrollView>
        </ImageBackground>
    );
}
