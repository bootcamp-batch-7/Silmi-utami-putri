import { View, ImageBackground, Image, ScrollView } from 'react-native'
import React from 'react'
import Text from '../../components/Text/index'
import images from '../../assets/img/index'
import { WIDTH, HEIGHT } from '../../assets/styles'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'


export default function Profile() {
    return (
        <ScrollView showsVerticalScrollIndicator = {false} contentContainerStyle={{height: HEIGHT}}>
            <View style={{ height: HEIGHT, backgroundColor: '#FFF', alignItems: 'center' }}>
                <ImageBackground source={images.BG_PROFILE} resizeMode='cover' style={{ width: WIDTH, height: 495, alignItems: 'center', paddingTop: 65, marginBottom: -220 }}>
                    <View>
                        <Image source={images.PERSON} style={{ width: 120, height: 120, borderRadius: 100, borderColor: '#FBD2A5', borderWidth: 3 }}></Image>
                    </View>
                    <View style={{ paddingTop: 20, alignItems: 'center' }}>
                        <Text color='#909090' fontSize={20} bold>Silmi Utami Putri</Text>
                        <Text color='#909090'>System Analyst</Text>
                    </View>
                </ImageBackground>

                <View style={{
                    width: 400,
                    borderRadius: 12,
                    marginTop: 20,
                    backgroundColor: '#FFF',
                    paddingLeft: 16,
                    paddingRight: 16,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 0.5 },
                    shadowOpacity: 0.15,
                    shadowRadius: 4,
                    elevation: 3
                }}>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text bold>ID</Text>
                            <Text color='#A7A7A7'>A12.2019.06269</Text>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text bold>Email</Text>
                            <Text color='#A7A7A7'>silmiutamiputri299@gmail.com</Text>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text bold>Date Of Birth</Text>
                            <Text color='#A7A7A7'>kepoo dehh</Text>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text bold>Gender</Text>
                            <Text color='#A7A7A7'>Female</Text>
                        </View>
                        <View style={{ borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <View style={{
                        marginTop: 20,
                        width: 400,
                        height: 90,
                        backgroundColor: '#FFF',
                        borderRadius: 12,
                        padding: 16,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 0.5 },
                        shadowOpacity: 0.15,
                        shadowRadius: 4,
                        elevation: 3
                    }}>
                        <View>
                            <Text bold>Team</Text>
                            <Text color='#A7A7A7'>Developer</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={images.PERSON1} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                            <Image source={images.PERSON2} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                            <Image source={images.PERSON5} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                            <View style={{ width: 35, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 90, backgroundColor: '#C16262', borderWidth: 1, borderColor: '#FFF' }}>
                                <Text color='#FFF'>+6</Text>
                            </View>

                        </View>

                    </View>
                </View>
                <View style={{
                    width: 400,
                    borderRadius: 12,
                    marginTop: 20,
                    backgroundColor: '#FFF',
                    paddingLeft: 16,
                    paddingRight: 16,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 0.5 },
                    shadowOpacity: 0.15,
                    shadowRadius: 4,
                    elevation: 3
                }}>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={images.GUARD} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                <Text bold>Privacy and Security</Text>
                            </View>
                            <Image source={images.ARROW}></Image>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={images.HELP} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                <Text bold>Help</Text>
                            </View>
                            <Image source={images.ARROW}></Image>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={images.ABOUT} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                <Text bold>About Us</Text>
                            </View>
                            <Image source={images.ARROW}></Image>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                    <View style={{ marginTop: 16 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={images.LOGOUT} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                <Text bold>Logout</Text>
                            </View>
                            <Image source={images.ARROW}></Image>
                        </View>
                        <View style={{ borderColor: '#D3D3D3', marginTop: 13 }}></View>
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Text bold>v0.01</Text>
                </View>


            </View>
        </ScrollView>
    )
}



