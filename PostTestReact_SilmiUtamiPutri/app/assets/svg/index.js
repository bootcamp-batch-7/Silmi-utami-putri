import HomeIcon from "./home.svg";
import ProfileIcon from "./profile.svg";
import PerformIcon from "./perform.svg";
import TaskIcon from "./task.svg";
import EyeIcon from "./eye.svg";
import EyeSlashIcon from "./eyeSlash.svg";

export { HomeIcon, ProfileIcon, PerformIcon, TaskIcon, EyeIcon, EyeSlashIcon };
