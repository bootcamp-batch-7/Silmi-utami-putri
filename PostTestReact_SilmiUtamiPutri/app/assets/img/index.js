export default {
  BG_PROFILE : require('./bgProfile.png'),
  PERSON : require('./person9.png'),
  PERSON1 : require('./person1.png'),
  PERSON2 : require('./person3.png'),
  PERSON5 : require('./person5.png'),
  GUARD : require('./guard.png'),
  ARROW : require('./Vector.png'),
  HELP : require('./help.png'),
  ABOUT : require('./about.png'),
  LOGOUT : require('./logout.png'),
  HOME : require('./home.png'),
  TASK : require('./task.png'),
  PERFORMANCE : require('./perform.png'),
  PROFILE : require('./profile.png'),
  ADD: require('./Add.png'),
  BG_LOGIN: require('./bgscreen_1.png')
}