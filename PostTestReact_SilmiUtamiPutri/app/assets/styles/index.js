import { Dimensions } from "react-native";

const Fonts = {
  Nunito: {
    Bold: "Nunito-Bold",
    Regular: "Nunito-Regular",
    SemiBold: "Nunito-SemiBold",
    BoldItalic: "Nunito-BoldItalic",
    SemiBoldItalic: "Nunito-SemiBoldItalic",
  },
};

const WIDTH = Dimensions.get("screen").width;
const HEIGHT = Dimensions.get("screen").height;
const HEIGHT_WD = Dimensions.get("window").height;

export { Fonts, WIDTH, HEIGHT, HEIGHT_WD };
